import * as amqp from 'amqplib'

const url = 'amqp://minecraft-rabbitmq'

// RabbitMQ Work Queue
export const initRMQChannel = async (): Promise<void> => {
  try {
    const connection = await amqp.connect(url)
    const channel = await connection.createChannel()
    const queue = 'testqueue'

    await channel.assertQueue(queue, { durable: true })

    // Not to give more than one message to a worker at a time
    channel.prefetch(1)

    console.log(' [*] Waiting for messages in %s. To exit press CTRL+C', queue)

    channel.consume(
      queue,
      (msg) => {
        if (msg !== null) {
          // console.log(, )

          const message = msg.content.toString()

          console.log(' [x] Received %s', JSON.parse(message))

          setTimeout(function () {
            console.log(' [x] Done %s', JSON.parse(message))
            // Acknowledge the message if noAck is false
            channel.ack(msg)
          }, 5 * 1000)
        }
      },
      { noAck: false }
    )
  } catch (error) {
    console.error('Failed to initialize RabbitMQ channel:', error)
  }
}

// TEST RabbitMQ RPC
export async function startMinecraftServer() {
  const connection = await amqp.connect(url)
  const channel = await connection.createChannel()

  const queue = 'start_server_queue'

  await channel.assertQueue(queue, { durable: true })
  channel.consume(
    queue,
    async (msg) => {
      if (!msg) return
      const correlationId = msg.properties.correlationId
      const replyTo = msg.properties.replyTo

      // Simulate starting the Minecraft server
      console.log('Starting Minecraft server...')
      await startMinecraftServerProcess() // This function should start your server and wait until it's ready

      channel.sendToQueue(replyTo, Buffer.from('Server is ready'), {
        correlationId: correlationId,
      })
      channel.ack(msg)
    },
    { noAck: false }
  )
}

async function startMinecraftServerProcess() {
  return new Promise<void>((resolve) => {
    setTimeout(() => {
      console.log('Minecraft server started.')
      resolve()
    }, 20 * 1000)
  })
}
