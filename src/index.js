const net = require('net')

const LOCAL_HOST = '127.0.0.1'
// Define the backend servers based on domains
const backends = {
  'aaa.hostcraft.top': { host: LOCAL_HOST, port: 3306 },
  'bbb.hostcraft.top': { host: LOCAL_HOST, port: 3307 },
}

// Function to extract domain from TCP data
function extractDomain(data) {
  // Assuming the domain name is embedded at the start of the data and is separated by a delimiter (e.g., a space)
  const dataStr = data.toString()
  const domain = dataStr.split(' ')[0] // Adjust this logic based on your protocol
  return domain
}

// Create a TCP server
const server = net.createServer((clientSocket) => {
  clientSocket.once('data', (data) => {
    const domain = extractDomain(data)

    if (!domain) return console.log('no domain detected')

    // VULNERABILITY
    // if domain includes hostname
    const host = Object.keys(backends).find((host) => domain.includes(host))

    if (host) {
      const backend = backends[host]
      const backendSocket = net.connect(backend.port, backend.host, () => {
        backendSocket.write(data)
        clientSocket.pipe(backendSocket).pipe(clientSocket)
      })

      backendSocket.on('error', (err) => {
        console.error(`Backend connection error: ${err.message}`)
        clientSocket.end()
      })
    } else {
      console.error(`No backend found for domain: ${domain}`)
      clientSocket.end()
    }
  })

  clientSocket.on('error', (err) => {
    console.error(`Client connection error: ${err.message}`)
  })
})

const PORT = 25565

server.listen(PORT, () => {
  console.log(`TCP reverse proxy server listening on port ${PORT}`)
})
